import { EventEmitter } from 'events';

describe('main route/middleware logic', () => {
    let route: any, data: any, helpers: any, req: any, res: any;

    beforeEach(() => {
        jest.resetModules();

        jest.mock('./utils/data.in-memory');
        jest.mock('./utils/helpers', () => {
            const stream = { pipe: jest.fn() };
            return {
                getToken: jest.fn(req => 'unique-token'),
                getUserId: jest.fn(token => 'user-1'),
                getStream: () => stream,
            };
        });

        route = require('./stream').default;
        data = require('./utils/data.in-memory').default;
        helpers = require('./utils/helpers');

        req = {};
        res = new EventEmitter();
        Object.assign(res, { end: jest.fn() });
    });

    describe('when limit exceeded', () => {
        beforeEach(() => {
            // ARRANGE
            data.getCurrentStreamCount.mockImplementation(() => Promise.resolve(4));
            data.getConcurrentStreamLimit.mockImplementation(() => Promise.resolve(3));
        });

        test('sets statusCode to 429', async () => {
            // ACT
            await route(req, res);

            // ASSERT
            expect(res.statusCode).toBe(429);
            expect(res.end).toHaveBeenCalledTimes(1);
        });
    });

    describe('when within limit', () => {
        beforeEach(() => {
            // ARRANGE
            data.getCurrentStreamCount.mockImplementation(() => Promise.resolve(2));
            data.getConcurrentStreamLimit.mockImplementation(() => Promise.resolve(3));
        });

        test('pipes the stream to the response', async () => {
            // ACT
            await route(req, res);

            // ASSERT
            expect(helpers.getStream().pipe).toHaveBeenCalledTimes(1);
            expect(helpers.getStream().pipe).toHaveBeenCalledWith(res);
        });

        test('increments the users stream count', async () => {
            // ACT
            await route(req, res);

            // ASSERT
            expect(data.incrementStreamCount).toHaveBeenCalledTimes(1);
            expect(data.incrementStreamCount).toHaveBeenCalledWith('user-1');
        });

        test('decrements the users stream count when the response closes', async () => {
            // ACT
            await route(req, res);
            res.emit('close');

            // ASSERT
            expect(data.decrementStreamCount).toHaveBeenCalledTimes(1);
            expect(data.decrementStreamCount).toHaveBeenCalledWith('user-1');
        });

        test('decrements the users stream count when the response errors', async () => {
            // ACT
            await route(req, res);
            res.emit('error');

            // ASSERT
            expect(data.decrementStreamCount).toHaveBeenCalledTimes(1);
            expect(data.decrementStreamCount).toHaveBeenCalledWith('user-1');
        });

        test('decrements the users stream count when the response finishes', async () => {
            // ACT
            await route(req, res);
            res.emit('finish');

            // ASSERT
            expect(data.decrementStreamCount).toHaveBeenCalledTimes(1);
            expect(data.decrementStreamCount).toHaveBeenCalledWith('user-1');
        });

        test('decrements the users stream count by 1 when the response closes, error and/or finishes', async () => {
            // ACT
            await route(req, res);
            res.emit('close');
            res.emit('error');
            res.emit('finish');

            // ASSERT
            expect(data.decrementStreamCount).toHaveBeenCalledTimes(1);
            expect(data.decrementStreamCount).toHaveBeenCalledWith('user-1');
        });
    });
});
