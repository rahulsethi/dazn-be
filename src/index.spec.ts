beforeEach(() => {
    jest.resetModules();

    jest.mock('./server', () => {
        return { server: { listen: jest.fn() } };
    });
});

describe('index', () => {
    test('calls listen() with `undefined` when PORT not set on environment', () => {
        // ARRANGE
        const { server } = require('./server');

        // ACT
        require('./index');

        // ASSERT
        expect(server.listen).toHaveBeenCalledTimes(1);
        expect(server.listen).toHaveBeenCalledWith(0, expect.any(Function));
    });

    test('calls listen() with port number set by environment', () => {
        // ARRANGE
        const { server } = require('./server');
        process.env.PORT = '34567';

        // ACT
        require('./index');

        // ASSERT
        expect(server.listen).toHaveBeenCalledTimes(1);
        expect(server.listen).toHaveBeenCalledWith(34567, expect.any(Function));
    });
});
