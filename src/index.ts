import { server } from './server';

server.listen(process.env.PORT || 0, () => {
    console.log(`Listening on port ${server.address().port}...`);
});
