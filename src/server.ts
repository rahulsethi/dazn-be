import { createServer, Server } from 'http';
import onRequestStream from './stream';

export const server: Server = createServer((req, res) => {
    const { url } = req;
    switch (url) {
        case '/':
            onRequestStream(req, res);
            break;
        default:
            res.statusCode = 404;
            res.end('Not Found');
    }
});
