import { IncomingMessage, ServerResponse } from 'http';
import data from './utils/data.in-memory';
import { getToken, getUserId, getStream } from './utils/helpers';
import { once } from './utils/once';

export default async (req: IncomingMessage, res: ServerResponse) => {
    const token = await getToken(req);
    const userId = await getUserId(token);
    const limit = data.getConcurrentStreamLimit(userId);

    if ((await data.getCurrentStreamCount(userId)) > (await limit)) {
        res.statusCode = 429;
        res.end(`You can only consume ${await limit} streams at once`);

        return;
    }

    getStream().pipe(res);
    data.incrementStreamCount(userId);

    const onClose = once(() => data.decrementStreamCount(userId));
    res.on('close', onClose);
    res.on('error', onClose);
    res.on('finish', onClose);
};
