export interface Data {
    getCurrentStreamCount(userId: string): Promise<number>;
    getConcurrentStreamLimit(userId: string): Promise<number>;
    incrementStreamCount(userId: string): Promise<void>;
    decrementStreamCount(userId: string): Promise<void>;
}
