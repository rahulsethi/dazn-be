import { once } from './once';

describe('once()', () => {
    test('wraps a function and prevents it being called more than once', () => {
        const fn = jest.fn();
        const wrapper = once(fn);

        wrapper();
        wrapper();
        wrapper();

        expect(fn).toHaveBeenCalledTimes(1);
        expect(fn).toHaveBeenCalledWith();
    });

    test('does not call the input function when wrapping', () => {
        const fn = jest.fn();
        const wrapper = once(fn);

        expect(fn).toHaveBeenCalledTimes(0);
    });

    test('passes all args to the input function', () => {
        const fn = jest.fn();
        const wrapper = once(fn);

        wrapper({ one: 1 }, { two: 2 }, { three: 3 });

        expect(fn).toHaveBeenCalledTimes(1);
        expect(fn).toHaveBeenCalledWith({ one: 1 }, { two: 2 }, { three: 3 });
    });
});
