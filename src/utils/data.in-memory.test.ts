import { Data } from './data';

describe('in-memory implementation of Data interface', () => {
    let data: Data;

    beforeEach(() => {
        jest.resetModules();
        data = require('./data.in-memory').default;
    });

    test('getCurrentStreamCount() initially returns 0', async () => {
        // ACT
        const result = await data.getCurrentStreamCount('user-1');

        // ASSERT
        expect(result).toBe(0);
    });

    test('getConcurrentStreamLimit() returns 3', async () => {
        // ACT
        const result = await data.getConcurrentStreamLimit('user-1');

        // ASSERT
        expect(result).toBe(3);
    });

    test('incrementStreamCount() increases result of getCurrentStreamCount() by 1', async () => {
        // ARRANGE
        const counts = [];
        counts.push(await data.getCurrentStreamCount('user-1'));

        // ACT
        await data.incrementStreamCount('user-1');

        // ASSERT
        counts.push(await data.getCurrentStreamCount('user-1'));
        expect(counts).toEqual([0, 1]);
    });

    test('decrementStreamCount() decreases result of getCurrentStreamCount() by 1', async () => {
        // ARRANGE
        const counts = [];
        counts.push(await data.getCurrentStreamCount('user-1'));
        await data.incrementStreamCount('user-1');
        counts.push(await data.getCurrentStreamCount('user-1'));

        // ACT
        await data.decrementStreamCount('user-1');

        // ASSERT
        counts.push(await data.getCurrentStreamCount('user-1'));
        expect(counts).toEqual([0, 1, 0]);
    });

    test('decrementStreamCount() does not decrement past zero', async () => {
        // ARRANGE
        const counts = [];
        counts.push(await data.getCurrentStreamCount('user-1'));

        // ACT
        await data.decrementStreamCount('user-1');

        // ASSERT
        counts.push(await data.getCurrentStreamCount('user-1'));
        expect(counts).toEqual([0, 0]);
    });

    test('(increment|decrement)StreamCount() does not affect getCurrentStreamCount() of other users', async () => {
        // ARRANGE
        const user1_counts = [];
        const user2_counts = [];
        user1_counts.push(await data.getCurrentStreamCount('user-1'));
        user2_counts.push(await data.getCurrentStreamCount('user-2'));

        // ACT
        await data.incrementStreamCount('user-1');
        user1_counts.push(await data.getCurrentStreamCount('user-1'));
        user2_counts.push(await data.getCurrentStreamCount('user-2'));
        await data.incrementStreamCount('user-1');
        user1_counts.push(await data.getCurrentStreamCount('user-1'));
        user2_counts.push(await data.getCurrentStreamCount('user-2'));
        await data.decrementStreamCount('user-1');
        user1_counts.push(await data.getCurrentStreamCount('user-1'));
        user2_counts.push(await data.getCurrentStreamCount('user-2'));

        // ASSERT
        expect(user1_counts).toEqual([0, 1, 2, 1]);
        expect(user2_counts).toEqual([0, 0, 0, 0]);
    });
});
