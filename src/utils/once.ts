export const noop = () => {};
export const once = (fn: Function) => {
    return (...args: any[]) => {
        fn(...args);
        fn = noop;
    };
};
