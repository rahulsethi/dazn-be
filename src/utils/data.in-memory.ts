import { Data } from './data';

const currentStreamCounts = new Map<string, number>();
const changeStreamCount = async (userId: string, increment = 1): Promise<void> => {
    const current = currentStreamCounts.get(userId) || 0;
    currentStreamCounts.set(userId, Math.max(0, current + increment));
};

const data: Data = {
    getCurrentStreamCount: async userId => currentStreamCounts.get(userId) || 0,
    getConcurrentStreamLimit: async userId => 3,
    incrementStreamCount: async userId => changeStreamCount(userId, 1),
    decrementStreamCount: async userId => changeStreamCount(userId, -1),
};

export default data;
