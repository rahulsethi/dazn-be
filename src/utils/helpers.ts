import { IncomingMessage } from 'http';
import { Readable } from 'stream';

export const getToken = async (req: IncomingMessage): Promise<string> => '';
export const getUserId = async (token: string): Promise<string> => token;
export const getStream = () => new Readable();
