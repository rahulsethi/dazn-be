jest.mock('http', () => {
    return { createServer: jest.fn(fn => fn) };
});
jest.mock('./stream');

const { server } = require('./server');
const onRequestStream = require('./stream').default;

afterEach(() => {
    onRequestStream.mockReset();
});

describe('server', () => {
    test('hands off handling to main route for requests for "/"', () => {
        // ARRANGE
        const req = { url: '/' };
        const res = { end: jest.fn() };

        // ACT
        server(req, res);

        // ASSERT
        expect(res.end).not.toHaveBeenCalled();
        expect(onRequestStream).toHaveBeenCalledTimes(1);
        expect(onRequestStream).toHaveBeenCalledWith(req, res);
    });

    test('handles all other requests as 404s', () => {
        // ARRANGE
        const req: any = { url: '/other' };
        const res: any = { end: jest.fn() };

        // ACT
        server(req, res);

        // ASSERT
        expect(res.statusCode).toBe(404);
        expect(res.end).toHaveBeenCalledTimes(1);
    });
});
