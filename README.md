# DAZN Back-End Node.js Coding Challenge

**Attempted by: Rahul Sethi**

## Problem

*   Build a service in Node.js that exposes an API which can be consumed from any
    client. This service must check how many video streams a given user is watching
    and prevent a user watching more than 3 video streams concurrently.

## Initial Thoughts

*   Each request to the server needs to be attributed to a user.

    *   Possible identifiers include: IP adddress, cookie, or token.
        *   IP address would be the simplest implementation but could be a greedy categoriser and therefore sub-optimal.
        *   Setting a cookie to unknown users can be easily circumvented by a client clearing cookies or simply not respecting `Set-Cookie` headers.
        *   Requiring a user to create an API key for use in the `Authorization` header requires an additional step for the user but, if authenticated properly before granting, allows a simple mechanism for categorising requests.
        *   **Conclusion:** Use tokens.

## Implementation

*   I have abstracted implementation details around managing user state to modules implementing the Data interface
    *   All methods are purposely async
    *   Currently, the only module implementing the interface implements the methods (sychronously) using in-memory data
    *   It should be relatively easy to write an implementation of this interface that communicates with a Redis instance (taking advantage of the methods' async nature)
*   **Assumption:** Creation and validation of tokens/userId's is handled by a seperate service

## Notes

*   `prettier` my editor has `prettier` installed and configured
    *   I have chosen not to install is locally and include a local config to reduce irrelevant files/clutter
*   I am working on a very slow connection, therefore using `npm install ... --prefer-offline`
    *   I had some issues (with known solutions) getting `jest` to transpile when testing, so it currently tests build output (not ideal, but it works)
        *   Known solution: install `babel-jest` which requires `babel-core` v6 or v7. I had `babel-core@6.2.x` but `@babel/preset-typescript@7.0.0-beta.40` in my cache -- note the mismatch
